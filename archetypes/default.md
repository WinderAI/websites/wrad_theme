+++
draft = true
date = "{{ .Date }}"
publishdate = "{{ .Date }}"
lastmod = "{{ .Date }}"
title = "{{ replace .Name "-" " " | title }}"

description = ""

summary = ""

tags = []

keywords = []

includes_math = false

[amp]
    elements = []

[author]
    name = ""
    homepage = ""

[image]
    src = ""

[ogp]
    title = ""
    url = ""
    description = ""
    image = ""
    site = ""

[twitter]
    title = ""
    url = ""
    description = ""
    image = ""
    site = ""

[sitemap]
    changefreq = "monthly"
    priority = 0.5
    filename = "sitemap.xml"
+++