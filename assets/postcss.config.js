module.exports = {
    plugins: [
        require('cssnano')({
            preset: ["default", { discardComments: { removeAll: true } }],
        }),
        require('@fullhuman/postcss-purgecss')({
            content: ['./hugo_stats.json'],
            defaultExtractor: (content) => {
                let els = JSON.parse(content).htmlElements;
                return els.tags.concat(els.classes, els.ids);
            },
            whitelistPatterns: [/cc-.*/],
            // content: ['winder.ai/index.html']
        }),
    ]
};